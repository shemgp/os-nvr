// Copyright 2020-2021 The OS-NVR Authors.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; version 2.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package ffmpeg

import (
	"context"
	"errors"
	"fmt"
	"image"
	"io/ioutil"
	"nvr/pkg/log"
	"os"
	"os/exec"
	"reflect"
	"strconv"
	"strings"
	"testing"
	"time"
)

func TestFakeProcess(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}
	if os.Getenv("SLEEP") == "1" {
		time.Sleep(1 * time.Hour)
	}

	fmt.Fprintf(os.Stdout, "%v", "out")
	fmt.Fprintf(os.Stderr, "%v", "err")

	os.Exit(0)
}

func fakeExecCommand(env ...string) *exec.Cmd {
	cs := []string{"-test.run=TestFakeProcess"}
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_TEST_PROCESS=1"}
	cmd.Env = append(cmd.Env, env...)
	return cmd
}

func TestProcess(t *testing.T) {
	t.Run("running", func(t *testing.T) {
		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		p := NewProcess(fakeExecCommand())
		err := p.Start(ctx)
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
	})
	t.Run("startWithLogger", func(t *testing.T) {
		t.Run("working", func(t *testing.T) {
			ctx, cancel := context.WithCancel(context.Background())

			logger := log.NewMockLogger()
			go logger.Start(ctx)
			feed, cancel2 := logger.Subscribe()

			p := NewProcess(fakeExecCommand())
			p.SetTimeout(0)
			p.SetPrefix("test ")
			p.SetStdoutLogger(logger)
			p.SetStderrLogger(logger)

			if err := p.Start(ctx); err != nil {
				t.Fatalf("failed to start %v", err)
			}

			compareOutput := func(input log.Log) {
				output1 := "test stdout: out\n"
				output2 := "test stderr: err\n"
				switch {
				case input.Msg == output1:
				case input.Msg == output2:
				default:
					t.Fatal("output does not match")
				}
			}

			compareOutput(<-feed)
			compareOutput(<-feed)
			cancel2()

			cancel()
		})
	})
	_, pw, err := os.Pipe()
	if err != nil {
		t.Fatal("could not create pipe")
	}

	t.Run("stdoutErr", func(t *testing.T) {
		p := process{cmd: fakeExecCommand()}
		p.cmd.Stdout = pw
		p.SetStdoutLogger(&log.Logger{})

		if err := p.Start(context.Background()); err == nil {
			t.Fatalf("nil")
		}
	})
	t.Run("stderrErr", func(t *testing.T) {
		p := process{cmd: fakeExecCommand()}
		p.cmd.Stderr = pw
		p.SetStderrLogger(&log.Logger{})

		if err := p.Start(context.Background()); err == nil {
			t.Fatalf("nil")
		}
	})
}

func TestMakePipe(t *testing.T) {
	t.Run("working", func(t *testing.T) {
		tempDir, err := ioutil.TempDir("", "")
		if err != nil {
			t.Fatalf("could not create tempoary directory: %v", err)
		}
		defer os.RemoveAll(tempDir)

		pipePath := tempDir + "/pipe.fifo"
		if err := MakePipe(pipePath); err != nil {
			t.Fatalf("could not create pipe: %v", err)
		}

		if _, err := os.Stat(pipePath); os.IsNotExist(err) {
			t.Fatal("pipe were not created")
		}
	})
	t.Run("MkfifoErr", func(t *testing.T) {
		if err := MakePipe(""); err == nil {
			t.Fatal("nil")
		}
	})
}

func TestShellProcessSize(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}
	fmt.Fprint(os.Stderr, `
		Stream #0:0: Video: h264 (Main), yuv420p(progressive), 720x1280 fps, 30.00
	`)
}

func fakeExecCommandSize(...string) *exec.Cmd {
	cs := []string{"-test.run=TestShellProcessSize"}
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_TEST_PROCESS=1"}
	return cmd
}

func TestShellProcessNoOutput(t *testing.T) {}

func fakeExecCommandNoOutput(...string) *exec.Cmd {
	cs := []string{"-test.run=TestShellProcessNoOutput"}
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_TEST_PROCESS=1"}
	return cmd
}

func TestSizeFromStream(t *testing.T) {
	t.Run("working", func(t *testing.T) {
		f := New("")
		f.command = fakeExecCommandSize

		actual, err := f.SizeFromStream("")
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}

		expected := "720x1280"
		if expected != actual {
			t.Fatalf("expected: %v, got: %v", expected, actual)
		}
	})
	t.Run("runErr", func(t *testing.T) {
		f := New("")
		if _, err := f.SizeFromStream(""); err == nil {
			t.Fatal("nil")
		}
	})
	t.Run("regexErr", func(t *testing.T) {
		f := New("")
		f.command = fakeExecCommandNoOutput

		if _, err := f.SizeFromStream(""); err == nil {
			t.Fatal("nil")
		}
	})
}

func TestShellProcessDuration(t *testing.T) {
	if os.Getenv("GO_TEST_PROCESS") != "1" {
		return
	}
	fmt.Fprint(os.Stderr, `
		Duration: 01:02:59.99, start: 0.000000, bitrate: 614 kb/s
	`)
}

func fakeExecCommandDuration(...string) *exec.Cmd {
	cs := []string{"-test.run=TestShellProcessDuration"}
	cmd := exec.Command(os.Args[0], cs...)
	cmd.Env = []string{"GO_TEST_PROCESS=1"}
	return cmd
}

func TestVideoDuration(t *testing.T) {
	t.Run("working", func(t *testing.T) {
		f := New("")
		f.command = fakeExecCommandDuration

		output, err := f.VideoDuration("")
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
		actual := fmt.Sprintf("%v", output)
		expected := "1h2m59.99s"
		if expected != actual {
			t.Fatalf("expected: %v, got: %v", expected, actual)
		}
	})
	t.Run("runErr", func(t *testing.T) {
		f := New("")
		if _, err := f.VideoDuration(""); err == nil {
			t.Fatal("nil")
		}
	})
	t.Run("regexErr", func(t *testing.T) {
		f := New("")
		f.command = fakeExecCommandNoOutput

		if _, err := f.VideoDuration(""); err == nil {
			t.Fatal("nil")
		}
	})
}

func imageToText(img image.Image) string {
	var text string
	max := img.Bounds().Max
	for y := 0; y < max.Y; y++ {
		text += "\n"
		for x := 0; x < max.X; x++ {
			_, _, _, a := img.At(x, y).RGBA()
			if a == 0 {
				text += "_"
			} else {
				text += "X"
			}
		}
	}
	return text
}

func TestPolygonToAbs(t *testing.T) {
	polygon := Polygon{
		Point{5, 10},
		Point{15, 20},
		Point{25, 30},
	}
	actual := fmt.Sprintf("%v", polygon.ToAbs(400, 200))
	expected := "[[20 20] [60 40] [100 60]]"

	if expected != actual {
		t.Fatalf("\nexpected:\n%v\ngot:\n%v", expected, actual)
	}
}

func TestCreateMask(t *testing.T) {
	cases := []struct {
		name     string
		input    Polygon
		expected string
	}{
		{
			"triangle",
			Polygon{
				{3, 1},
				{6, 6},
				{0, 6},
			},
			`
			_______
			_______
			___X___
			__XXX__
			__XXX__
			_XXXXX_
			_______`,
		},
		{
			"octagon",
			Polygon{
				{2, 0},
				{5, 0},
				{7, 3},
				{7, 4},
				{4, 7},
				{0, 4},
				{0, 2},
			},
			`
			__XXX__
			_XXXXX_
			XXXXXXX
			XXXXXXX
			XXXXXXX
			_XXXXX_
			__XXX__`,
		},
		{
			"inverted", // Lines cross over themselves at the bottom.
			Polygon{
				{7, 0},
				{7, 7},
				{1, 5},
				{6, 5},
				{0, 7},
				{0, 0},
			},
			`
			XXXXXXX
			XXXXXXX
			XXXXXXX
			XXXXXXX
			XXXXXXX
			X_____X
			XXX_XXX`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			mask := CreateMask(7, 7, tc.input)
			actual := imageToText(mask)
			expected := strings.ReplaceAll(tc.expected, "\t", "")

			if expected != actual {
				t.Fatalf("\nexpected:\n%v\ngot:\n%v", expected, actual)
			}
		})
	}
}

func TestCreateInvertedMask(t *testing.T) {
	cases := []struct {
		name     string
		input    Polygon
		expected string
	}{
		{
			"triangle",
			Polygon{
				{3, 1},
				{6, 6},
				{0, 6},
			},
			`
			XXXXXXX
			XXXXXXX
			XXX_XXX
			XX___XX
			XX___XX
			X_____X
			XXXXXXX`,
		},
		{
			"octagon",
			Polygon{
				{2, 0},
				{5, 0},
				{7, 3},
				{7, 4},
				{4, 7},
				{0, 4},
				{0, 2},
			},
			`
			XX___XX
			X_____X
			_______
			_______
			_______
			X_____X
			XX___XX`,
		},
		{
			"inverted", // Lines cross over themselves at the bottom.
			Polygon{
				{7, 0},
				{7, 7},
				{1, 5},
				{6, 5},
				{0, 7},
				{0, 0},
			},
			`
			_______
			_______
			_______
			_______
			_______
			_XXXXX_
			___X___`,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			mask := CreateInvertedMask(7, 7, tc.input)
			actual := imageToText(mask)
			expected := strings.ReplaceAll(tc.expected, "\t", "")

			if expected != actual {
				t.Fatalf("\nexpected:\n%v\ngot:\n%v", expected, actual)
			}
		})
	}
}

func TestSaveImage(t *testing.T) {
	t.Run("working", func(t *testing.T) {
		tempDir, err := ioutil.TempDir("", "")
		if err != nil {
			t.Fatalf("could not create tempoary directory: %v", err)
		}
		defer os.RemoveAll(tempDir)

		imgPath := tempDir + "/img.png"
		img := image.NewAlpha(image.Rect(0, 0, 1, 1))

		if err := SaveImage(imgPath, img); err != nil {
			t.Fatalf("could not save image: %v", err)
		}

		if _, err := os.Stat(imgPath); os.IsNotExist(err) {
			t.Fatal("image were not created")
		}
	})
	t.Run("createErr", func(t *testing.T) {
		img := image.NewAlpha(image.Rect(0, 0, 1, 1))
		if err := SaveImage("", img); err == nil {
			t.Fatal("nil")
		}
	})
	t.Run("encodeErr", func(t *testing.T) {
		tempDir, err := ioutil.TempDir("", "")
		if err != nil {
			t.Fatalf("could not create tempoary directory: %v", err)
		}

		imgPath := tempDir + "/img.png"
		file, err := os.Create(imgPath)
		if err != nil {
			t.Fatalf("could not create image: %v", err)
		}
		defer file.Close()

		img := image.NewAlpha(image.Rect(0, 0, 1, 1))
		img.Rect = image.Rectangle{}
		if err := SaveImage(imgPath, img); err == nil {
			t.Fatal("nil")
		}
	})
}

func TestParseArgs(t *testing.T) {
	cases := []struct {
		name     string
		input    string
		expected []string
	}{
		{"1", "1 2 3 4", []string{"1", "2", "3", "4"}},
		//{"2", "1 '2 3' 4", []string{"1", "2 3", "4"}}, Not implemented.
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			actual := ParseArgs(tc.input)

			if !reflect.DeepEqual(actual, tc.expected) {
				t.Fatalf("expected: %v, got: %v", tc.expected, actual)
			}
		})
	}
}

func TestWaitForKeyframe(t *testing.T) {
	cases := []struct {
		name        string
		input       string
		nSegments   int
		expected    time.Duration
		expectedErr error
	}{
		{
			name:        "empty",
			input:       "",
			nSegments:   1,
			expected:    0,
			expectedErr: ErrInvalidFile,
		},
		{
			name: "singleFrame",
			input: `
INPUT
#EXTM3U
#EXT-X-VERSION:3
#EXT-X-ALLOW-CACHE:NO
#EXT-X-TARGETDURATION:2
#EXT-X-MEDIA-SEQUENCE:251
#EXTINF:4.250000,
10.ts
#EXTINF:3.500000,
11.ts
`,
			nSegments:   1,
			expected:    3500 * time.Millisecond,
			expectedErr: nil,
		},
		{
			name: "multipleFrames",
			input: `
#EXTINF:9.000000,
9.ts
#EXTINF:4.250000,
10.ts
#EXTINF:3.500000,
11.ts
`,
			nSegments:   2,
			expected:    7750 * time.Millisecond,
			expectedErr: nil,
		},
		{
			name: "invalidLine",
			input: `
1,
2
`,
			nSegments:   1,
			expected:    0,
			expectedErr: ErrInvalidFile,
		},
		{
			name: "invalidDuration",
			input: `
				#EXTINF:3.5#0000,
				11.ts
				`,
			nSegments:   1,
			expected:    0,
			expectedErr: strconv.ErrSyntax,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			tempDir, err := ioutil.TempDir("", "")
			if err != nil {
				t.Fatalf("could not create tempoary directory: %v", err)
			}
			defer os.RemoveAll(tempDir)

			path := tempDir + "/test.m3u8"
			if err := ioutil.WriteFile(path, []byte(tc.input), 0o600); err != nil {
				t.Fatalf("could not write file: %v", err)
			}

			go func(nFrames int) {
				for i := 1; i <= nFrames; i++ {
					time.Sleep(10 * time.Millisecond)
					// Simulate new keyframe
					os.Chmod(path, 0o601)
				}
			}(tc.nSegments)

			actual, err := WaitForKeyframe(context.Background(), path, tc.nSegments)
			if !errors.Is(err, tc.expectedErr) {
				t.Fatalf("expected: %v, got: %v", tc.expectedErr, err)
			}

			if actual != tc.expected {
				t.Fatalf("expected: %v, got: %v", tc.expected, actual)
			}
		})
	}
	t.Run("addErr", func(t *testing.T) {
		tempDir, err := ioutil.TempDir("", "")
		if err != nil {
			t.Fatalf("could not create tempoary directory: %v", err)
		}
		defer os.RemoveAll(tempDir)

		_, err = WaitForKeyframe(context.Background(), "nil", 1)
		if err == nil {
			t.Fatal("expected: error got: nil")
		}
	})
	t.Run("canceled", func(t *testing.T) {
		tempDir, err := ioutil.TempDir("", "")
		if err != nil {
			t.Fatalf("could not create tempoary directory: %v", err)
		}
		defer os.RemoveAll(tempDir)

		path := tempDir + "/test.m3u8"
		if err := ioutil.WriteFile(path, []byte{}, 0o600); err != nil {
			t.Fatalf("could not write file: %v", err)
		}

		ctx, cancel := context.WithCancel(context.Background())
		cancel()

		_, err = WaitForKeyframe(ctx, path, 1)
		if err != nil {
			t.Fatalf("unexpected error: %v", err)
		}
	})
}

func TestFeedRateToDuration(t *testing.T) {
	cases := []struct {
		name     string
		input    string
		expected string
	}{
		{"one", "1", "1s"},
		{"two", "2", "500ms"},
		{"half", "0.5", "2s"},
	}
	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			output, err := FeedRateToDuration(tc.input)
			if err != nil {
				t.Fatalf("unexpected error: %v", err)
			}
			actual := fmt.Sprintf("%v", output)

			if tc.expected != actual {
				t.Fatalf("expected: %v, got: %v", tc.expected, actual)
			}
		})
	}
	t.Run("parseFloatErr", func(t *testing.T) {
		if _, err := FeedRateToDuration("nil"); err == nil {
			t.Fatal("expected: error got: nil")
		}
	})
}
