<img src="https://gitlab.com/osnvr/os-nvr-assets/-/raw/master/screenshots/recordings.png">

[Screenshots](https://gitlab.com/osnvr/os-nvr_assets/-/tree/master/screenshots)

## Overview

##### beta release.

OS-NVR is a mobile-friendly extensible CCTV system.

Frontend is written completely from scratch to give the best performance on mobile/low-end devices. Backend is written in Go using FFmpeg for video processing.

`OS-NVR` is a temporary name. [#2](https://gitlab.com/osnvr/os-nvr/-/issues/2)

Use [Issues]() for bug reports, feature requests and support.

## Documentation

- [Installation](./docs/1_Installation.md)
- [Configuration](./docs/2_Configuration.md)
- [Development](./docs/3_Development.md)
- [Object Detection](./addons/doods/README.md)
	
<br>

## Similar projects

- [ZoneMinder](https://github.com/ZoneMinder/ZoneMinder)
- [Moonfire NVR](https://github.com/scottlamb/moonfire-nvr)
- [Frigate](https://github.com/blakeblackshear/frigate)
- [Motion](https://github.com/Motion-Project/motion)[Eye](https://github.com/ccrisan/motioneye/)[OS](https://github.com/ccrisan/motioneyeos)

## License
All code is licensed under [GPL-2.0-only](LICENSE) 